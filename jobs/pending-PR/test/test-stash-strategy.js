var assert = require('assert');
var q = require('q');
var test_util = require('./util/util');
var stashStrategy = require('../providers/stash');

var mockFetchRequest, mockedDependencies;
beforeEach(function (done) {

  mockFetchRequest = {
    auth: {
      username: "myusername",
      password: "secretpassword"
    },
    sourceId: 'confluence',
    repository: { project: "CONF", repository: "confluence" },
    options: {
      baseUrl: "https://stash.atlassian.com"
    },
    team: [
      { username: "iloire" },
      { username: "dwillis" },
      { username: "mreis" }
    ]
  };

  mockedDependencies = {
    logger: console,
    easyRequest : {
      JSON : function (options, cb) {
        cb(null, {});
      }
    }
  };

  done();
});


describe('Stash strategy', function () {

  describe('required parameters', function () {

    it('options are required in repository', function (done) {
      delete mockFetchRequest.options;
      stashStrategy(mockFetchRequest, mockedDependencies).then(q.reject, function(err){
        assert.ok(err.indexOf('missing options') > -1);
        done();
      });
    });

    //it('requires repositories with at least one item', function (done) {
    //  mockedConfig.servers.confluence.repositories = [];
    //  pendingPR(mockedConfig, mockedDependencies, function(err){
    //    assert.ok(err);
    //    done();
    //  });
    //});
    //
    //it.only('requires repositories project field', function (done) {
    //  delete mockedConfig.servers.confluence.repositories[0].project;
    //
    //  pendingPR(mockedConfig, mockedDependencies, function(err){
    //    assert.ok(err);
    //    assert.ok(err.indexOf('missing project') > -1);
    //    done();
    //  });
    //});
    //
    //it('does not require repositories repository field', function (done) {
    //  delete mockedConfig.servers.confluence.repositories[0].repository;
    //
    //  pendingPR(mockedConfig, mockedDependencies, function(err){
    //    assert.ok(err);
    //    assert.ok(err.indexOf('no data') > -1);
    //    done();
    //  });
    //});
    //
    //it('requires baseUrl field', function (done) {
    //  delete mockedConfig.servers.confluence.options.baseUrl;
    //
    //  pendingPR(mockedConfig, mockedDependencies, function(err){
    //    assert.ok(err);
    //    assert.ok(err.indexOf('missing baseUrl') > -1);
    //    done();
    //  });
    //});
    //
    //it('warns about configuration changes', function(done) {
    //  mockedConfig.repositories = [];
    //
    //  pendingPR(mockedConfig, mockedDependencies, function(err){
    //    assert.ok(err);
    //    assert.ok(err.indexOf('new configuration format') > -1);
    //    done();
    //  });
    //});

  });

  describe('format', function () {

    it('returns data in the expected format', function (done) {
      mockedDependencies.easyRequest.JSON = function (options, cb) {
        var response = {
          size: 15,
          limit: 15,
          isLastPage: false,
          values: test_util.getFakeStashPR (10, 'iloire', ['dwillis']).
            concat(test_util.getFakeStashPR (5, 'dwillis', ['mreis'])).
            concat(test_util.getFakeStashPR (5, 'mreis', ['dwillis', 'iloire']))
        };
        cb(null, response);
      };

      stashStrategy(mockFetchRequest, mockedDependencies).then(function(users) {
        assert.equal(users.length, 3);

        assert.equal(users[0].user.username, 'iloire');
        assert.equal(users[0].PR, 5);

        assert.equal(users[1].user.username, 'dwillis');
        assert.equal(users[1].PR, 15);

        assert.equal(users[2].user.username, 'mreis');
        assert.equal(users[2].PR, 5);

        done();
      });
    });
  });

});
